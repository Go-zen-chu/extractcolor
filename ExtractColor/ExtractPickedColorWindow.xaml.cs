﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Collections;
using System.Windows.Media.Imaging;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using System.IO;
using Microsoft.Win32;
using System.Text;
using System;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Collections.Generic;
using OpenCvUtility.Image;
using System.Linq;

namespace ExtractColorWindow {
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class ExtractPickedColorWindow : Window {
        public Color PickedColor { get; set; }
        IplImage srcIplImage = null;
        IplImage dstIplImage = null;
        WriteableBitmap srcBitmap = null;
        string selectedTabName = null;
        bool processedFlg = false;
        string selectedImagePath = "";
        Rectangle selectedRectangle;
        Dictionary<string, Color> selectedRectColorDict = new Dictionary<string,Color>();

        public ExtractPickedColorWindow() {
            InitializeComponent();
            selectedTabName = selectOneTabItem.Name;
        }
        
        #region menu操作
        private void openImageMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.DefaultExt = "*.jpg";
            ofd.Filter = "PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|JPEG Files (*.jpeg)|*.jpeg";
            var defaultDirPath = Properties.Settings.Default.ImageDirectoryPath;
            ofd.InitialDirectory = (Directory.Exists(defaultDirPath)) ?
                defaultDirPath : System.IO.Path.Combine(getExtractColorPath(System.Environment.CurrentDirectory), "img");
            if (ofd.ShowDialog().Value)
            {
                var imagePath = ofd.FileName;
                srcIplImage = Cv.LoadImage(imagePath);
                dstIplImage = Cv.CreateImage(Cv.GetSize(srcIplImage), srcIplImage.Depth, srcIplImage.NChannels);
                srcBitmap = WriteableBitmapConverter.ToWriteableBitmap(srcIplImage, PixelFormats.Bgr24);
                mainImage.Source = srcBitmap;
                selectedImagePath = imagePath;
                Properties.Settings.Default.ImageDirectoryPath = System.IO.Path.GetDirectoryName(imagePath);
                Properties.Settings.Default.Save();
            }
        }

        private void saveImageMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists(selectedImagePath) == false) return;
            var parentPath = System.IO.Path.GetDirectoryName(selectedImagePath);
            var ext = System.IO.Path.GetExtension(selectedImagePath);
            var lower_ext = ext.ToLower();
            var imageFileName = System.IO.Path.GetFileNameWithoutExtension(selectedImagePath);
            var newImageName = new StringBuilder(imageFileName).Append("_")
                                                .Append(DateTime.Now.ToString("yyyyMMddHHmmss")).Append(ext).ToString();
            using (var stream = new FileStream(System.IO.Path.Combine(parentPath, newImageName), FileMode.Create, FileAccess.Write))
            {
                BitmapEncoder encoder = null;
                switch (lower_ext)
                {
                    case ".jpg":
                    case ".jpeg": encoder = new JpegBitmapEncoder(); break;
                    case ".png": encoder = new PngBitmapEncoder(); break;
                    case ".bmp": encoder = new BmpBitmapEncoder(); break;
                }
                if (encoder == null)
                {
                    messageLabel.Content = "Couldn't save file " + newImageName;
                    return;
                }
                encoder.Frames.Add(BitmapFrame.Create(mainImage.Source as WriteableBitmap));
                encoder.Save(stream);
                messageLabel.Content = "Saved " + newImageName;
            }
        }
        #endregion

        // 画像のクリック位置の色を選択
        private void mainImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            if (srcIplImage == null) return;
            // getPosition will be pixel value thanks to viewbox
            var clickedPoint = e.GetPosition(mainImage);
            PickedColor = General.GetPickedColor(srcBitmap, clickedPoint);

            if (selectedTabName == selectOneTabItem.Name)
            {
                var hsv = Hsv.FromRgb(PickedColor);
                selectOne_rgbLabel.Content = "RGB : " + string.Join(",", new byte[] { PickedColor.R, PickedColor.G, PickedColor.B });
                selectOne_hsvLabel.Content = "HSV : " + hsv.ToHsvString();
                selectOne_pickedColorRect.Fill = new SolidColorBrush(PickedColor);
            }
            else
            {
                if (selectedRectangle == null) return;
                var rectName = selectedRectangle.Name;
                selectedRectangle.Fill = new SolidColorBrush(PickedColor);
                if (selectedRectColorDict.ContainsKey(rectName))
                    selectedRectColorDict[rectName] = PickedColor;
                else
                    selectedRectColorDict.Add(rectName, PickedColor);
            }
        }

        // Gaussianフィルタをかける
        private void gaussianFilterCheckBox_Checked(object sender, RoutedEventArgs e) {
            if (srcIplImage == null) return;
            var blurStrength = (int)blurStrengthSlider.Value;
            // 奇数にする
            blurStrength = (blurStrength % 2 == 0) ? blurStrength + 1 : blurStrength;
            Cv.Smooth(srcIplImage, dstIplImage, SmoothType.Gaussian, blurStrength, blurStrength);
            mainImage.Source = WriteableBitmapConverter.ToWriteableBitmap(dstIplImage);
        }
        private void gaussianFilterCheckBox_Unchecked(object sender, RoutedEventArgs e) {
            if (srcIplImage == null) return;
            mainImage.Source = srcBitmap;
        }

        private void switchModeTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var tabItem = switchModeTabControl.SelectedItem as TabItem;
            if (tabItem != null) selectedTabName = tabItem.Name;
            processedFlg = false;
            mainImage.Source = srcBitmap;
        }

        private void selectOne_extractColorButton_Click(object sender, RoutedEventArgs e) {
            if (srcIplImage == null) return;
            if (processedFlg == false) {
                var hsv = Hsv.FromRgb(PickedColor);
                dstIplImage = ExtractColor.ExtractSingleColor((gaussianFilterCheckBox.IsChecked.Value) ? dstIplImage : srcIplImage,
                                                        hsv.H, hsv.S, hsv.V);
                mainImage.Source = WriteableBitmapConverter.ToWriteableBitmap(dstIplImage);
                selectOne_extractColorButton.Content = "Reset";
                processedFlg = true;
            } else {
                gaussianFilterCheckBox.IsChecked = false;
                mainImage.Source = srcBitmap;
                selectOne_extractColorButton.Content = "Process";
                processedFlg = false;
            }
        }

        private void selectMany_Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectedRectangle = sender as Rectangle;
        }
        private void selectMany_extractColorButton_Click(object sender, RoutedEventArgs e)
        {
            if (srcIplImage == null || selectedRectColorDict.Count == 0) return;
            if (processedFlg == false)
            {
                var inputIplImg = gaussianFilterCheckBox.IsChecked.Value ? dstIplImage : srcIplImage;
                var colors = selectedRectColorDict.Values;
                Dictionary<int, int> morpIntensityDict= null;
                if(morphologyToggleButton.IsChecked.Value){
                    morpIntensityDict = new Dictionary<int,int>();
                    foreach(var idx in Enumerable.Range(0, colors.Count)){
                        morpIntensityDict.Add(idx, 7);
                    }
                }
                dstIplImage = ExtractColor.ExtractColorsOnce(inputIplImg, colors, isSolid: planeModeToggleButton.IsChecked.Value, morphologyIntensityDict: morpIntensityDict);
                mainImage.Source = WriteableBitmapConverter.ToWriteableBitmap(dstIplImage);
                selectMany_ExtractColorButton.Content = "Reset";
                processedFlg = true;
            }
            else
            {
                gaussianFilterCheckBox.IsChecked = false;
                mainImage.Source = srcBitmap;
                selectMany_ExtractColorButton.Content = "Process";
                processedFlg = false;
            }
        }

        string getExtractColorPath(string anyPath)
        {
            var target = "ExtractColor";
            if (anyPath == null || anyPath.Contains(target) == false) return null;
            return anyPath.Substring(0, anyPath.LastIndexOf(target) + target.Length);
        }
        private void extractPickedColorWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (srcIplImage != null) Cv.ReleaseImage(srcIplImage);
            if (dstIplImage != null) Cv.ReleaseImage(dstIplImage);
        }

    }
}
